import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubjectsRoutingModule } from './subjects-routing.module';
import { SubjectsComponent } from './subjects.component';
import { IntroComponent } from './intro/intro.component';
import { NinjaFormationComponent } from './ninja-formation/ninja-formation.component';
import { BannerComponent } from './banner/banner.component';
import { DisciplineComponent } from './discipline/discipline.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    SubjectsRoutingModule,
    FlexLayoutModule
  ],
  declarations: [SubjectsComponent, IntroComponent, NinjaFormationComponent, BannerComponent, DisciplineComponent]
})
export class SubjectsModule { }

