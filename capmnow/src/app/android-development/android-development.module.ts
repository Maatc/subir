import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AndroidDevelopmentRoutingModule } from './android-development-routing.module';
import { AndroidDevelopmentComponent } from './android-development.component';
import { IntroComponent } from './intro/intro.component';
import { PresentationComponent } from './presentation/presentation.component';
import { ContentComponent } from './content/content.component';
import { BenefitsComponent } from './benefits/benefits.component';
import { CirclesComponent } from './circles/circles.component';
import { OnlineRegistrationComponent } from './online-registration/online-registration.component';
import { NotesComponent } from './notes/notes.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    AndroidDevelopmentRoutingModule,
    FlexLayoutModule
    
  ],
  declarations: [AndroidDevelopmentComponent, IntroComponent, PresentationComponent, ContentComponent, BenefitsComponent, CirclesComponent, OnlineRegistrationComponent, NotesComponent]
})
export class AndroidDevelopmentModule { }
