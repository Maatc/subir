import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AndroidDevelopmentComponent } from './android-development.component';

const routes: Routes = [
  {
    path: '',
    component: AndroidDevelopmentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AndroidDevelopmentRoutingModule { }
