import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { NinjaFormationRoutingModule } from './ninja-formation-routing.module';
import { NinjaFormationComponent } from './ninja-formation.component';
import { IntroComponent } from './intro/intro.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { KnowledgementComponent } from './knowledgement/knowledgement.component';
import { SharedModule } from '../shared/shared.module';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { TortasComponent } from './tortas/tortas.component';
import { FotoComponent } from './foto/foto.component';
import { CupcakeComponent } from './cupcake/cupcake.component';
import { DulceComponent } from './dulce/dulce.component';
import { BocaditosComponent } from './bocaditos/bocaditos.component';
import { PersonalizadoComponent } from './personalizado/personalizado.component';
import { HttpClientModule } from '@angular/common/http';
import { FlotanteComponent } from './flotante/flotante.component';


@NgModule({
  imports: [
    SharedModule,
    NinjaFormationRoutingModule ,
    FlexLayoutModule,
    Ng2CarouselamosModule,
    HttpClientModule
  ],
  declarations: [NinjaFormationComponent, IntroComponent, SubjectsComponent, KnowledgementComponent, TortasComponent,  FotoComponent, CupcakeComponent, DulceComponent, BocaditosComponent, PersonalizadoComponent, FlotanteComponent]
})
export class NinjaFormationModule { }
