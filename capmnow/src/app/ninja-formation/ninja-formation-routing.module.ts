import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NinjaFormationComponent } from './ninja-formation.component';
import { TortasComponent } from './tortas/tortas.component';
import { FotoComponent } from './foto/foto.component';
import { CupcakeComponent } from './cupcake/cupcake.component';
import { DulceComponent } from './dulce/dulce.component';
import { BocaditosComponent } from './bocaditos/bocaditos.component';
import { PersonalizadoComponent } from './personalizado/personalizado.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { FlotanteComponent } from './flotante/flotante.component';



const routes: Routes = [
  { path: '',component: NinjaFormationComponent},

  {
    path:'cupcake',component:CupcakeComponent,
     },

     {
      path:'subjects',component:SubjectsComponent,
       },
  {
    path:'dulceregalo',component:DulceComponent
  },
  {
    path:'tortaspedido',component:TortasComponent
  },
  {
    path:'fototortas',component:FotoComponent
  },
  {
    path:'bocaditos',component:BocaditosComponent
  },
  {
    path:'diseñospersonalizados',component:PersonalizadoComponent
  },
  {
    path:'flotante',component:SubjectsComponent
     },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NinjaFormationRoutingModule { }

