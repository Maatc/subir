import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-personalizado',
  templateUrl: './personalizado.component.html',
  styleUrls: ['./personalizado.component.scss']
})
export class PersonalizadoComponent implements OnInit {

  items:Array<any> =[];

  constructor() { 
  this.items =[
  { name:'/assets/images/serviciosadicionales/personalizado7-14.jpg'},
  { name:'/assets/images/serviciosadicionales/personzalizado1-14.jpg'},
  { name:'/assets/images/serviciosadicionales/personzalizado2-14.jpg'},
  { name:'/assets/images/serviciosadicionales/personzalizado3-14.jpg'},
  { name:'/assets/images/serviciosadicionales/personzalizado4-14.jpg'},
  { name:'/assets/images/serviciosadicionales/personzalizado5-14.jpg'},
  { name:'/assets/images/serviciosadicionales/personzalizado6-14.jpg'},
  ];
  
  }
  
  ngOnInit() {
  }
  
  }