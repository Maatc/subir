import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-foto',
  templateUrl: './foto.component.html',
  styleUrls: ['./foto.component.scss']
})
export class FotoComponent implements OnInit {

  items:Array<any> =[];

  constructor() { 
  this.items =[
  { name:'/assets/images/serviciosadicionales/fototorta1-15.jpg'},
  { name:'/assets/images/serviciosadicionales/fototorta2-15.jpg'},
  { name:'/assets/images/serviciosadicionales/fototorta3-15.jpg'},
  ];
  
  }
  
  ngOnInit() {
  }
  
  }