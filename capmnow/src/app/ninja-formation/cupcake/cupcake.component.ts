import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cupcake',
  templateUrl: './cupcake.component.html',
  styleUrls: ['./cupcake.component.scss']
})
export class CupcakeComponent implements OnInit {
  items:Array<any> =[];

  constructor() { 
  this.items =[
   { name:'/assets/images/serviciosadicionales/cupcake1-16.jpg'},
   { name:'/assets/images/serviciosadicionales/cupcake2-16.jpg'},
   { name:'/assets/images/serviciosadicionales/cupcake3-16.jpg'},
   { name:'/assets/images/serviciosadicionales/cupcake4-16.jpg'},
   { name:'/assets/images/serviciosadicionales/cupcake5-16.jpg'},
   { name:'/assets/images/serviciosadicionales/cupcake6-16.jpg'},
  ];
  
  }
  
  ngOnInit() {
  }
  
  }