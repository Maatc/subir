import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BocaditosComponent } from './bocaditos.component';

describe('BocaditosComponent', () => {
  let component: BocaditosComponent;
  let fixture: ComponentFixture<BocaditosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BocaditosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BocaditosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
