import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bocaditos',
  templateUrl: './bocaditos.component.html',
  styleUrls: ['./bocaditos.component.scss']
})
export class BocaditosComponent implements OnInit {

  items:Array<any> =[];

  constructor() { 
  this.items =[
   { name:'/assets/images/serviciosadicionales/bocaditos1-17.jpg'},
   { name:'/assets/images/serviciosadicionales/bocaditos2-17.jpg'},
   { name:'/assets/images/serviciosadicionales/bocaditos3-17.jpg'},
   { name:'/assets/images/serviciosadicionales/bocaditos4-17.jpg'},
   { name:'/assets/images/serviciosadicionales/bocaditos5-17.jpg'},
  ];
  
  }
  
  ngOnInit() {
  }
  
  }