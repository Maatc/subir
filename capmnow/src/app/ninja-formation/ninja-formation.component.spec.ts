import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NinjaFormationComponent } from './ninja-formation.component';

describe('NinjaFormationComponent', () => {
  let component: NinjaFormationComponent;
  let fixture: ComponentFixture<NinjaFormationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NinjaFormationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NinjaFormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
