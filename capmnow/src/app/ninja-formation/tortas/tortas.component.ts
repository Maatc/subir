import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tortas',
  templateUrl: './tortas.component.html',
  styleUrls: ['./tortas.component.scss']
})
export class TortasComponent implements OnInit {
  items:Array<any> =[];

  constructor() { 
this.items =[
  { name:'/assets/images/serviciosadicionales/tortas1-13.jpg'},
  { name:'/assets/images/serviciosadicionales/tortas2-13.jpg'},
  { name:'/assets/images/serviciosadicionales/tortas3-13.jpg'},
  { name:'/assets/images/serviciosadicionales/tortas4-13.jpg'},
  { name:'/assets/images/serviciosadicionales/tortas5-13.jpg'},
  { name:'/assets/images/serviciosadicionales/tortas6-13.jpg'},
  { name:'/assets/images/serviciosadicionales/tortas7-13.jpg'},
];

  }

  ngOnInit() {
  }

}