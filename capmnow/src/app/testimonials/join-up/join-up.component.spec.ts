import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinUpComponent } from './join-up.component';

describe('JoinUpComponent', () => {
  let component: JoinUpComponent;
  let fixture: ComponentFixture<JoinUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
