import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestimonialsRoutingModule } from './testimonials-routing.module';
import { TestimonialsComponent } from './testimonials.component';
import { IntroComponent } from './intro/intro.component';
import { TestimonialsXxxComponent } from './testimonials-xxx/testimonials-xxx.component';
import { PeopleComponent } from './people/people.component';
import { JoinUpComponent } from './join-up/join-up.component';
import { FlexLayoutModule } from '@angular/flex-layout'

@NgModule({
  imports: [
    CommonModule,
    TestimonialsRoutingModule,
    FlexLayoutModule

  ],
  declarations: [TestimonialsComponent, IntroComponent, TestimonialsXxxComponent, PeopleComponent, JoinUpComponent]
})
export class TestimonialsModule { }
