import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestimonialsXxxComponent } from './testimonials-xxx.component';

describe('TestimonialsXxxComponent', () => {
  let component: TestimonialsXxxComponent;
  let fixture: ComponentFixture<TestimonialsXxxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestimonialsXxxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestimonialsXxxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
