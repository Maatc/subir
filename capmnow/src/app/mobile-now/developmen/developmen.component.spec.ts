import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopmenComponent } from './developmen.component';

describe('DevelopmenComponent', () => {
  let component: DevelopmenComponent;
  let fixture: ComponentFixture<DevelopmenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevelopmenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopmenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
