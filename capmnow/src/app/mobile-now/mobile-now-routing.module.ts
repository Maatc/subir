import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MobileNowComponent } from './mobile-now.component';
import { Level1Component } from './level-1/level-1.component';

const routes: Routes = [
  {
    path:'',
    component:MobileNowComponent,
    pathMatch: 'full'
  },
  {
    path: 'nivel-1',
    component: Level1Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MobileNowRoutingModule { }
