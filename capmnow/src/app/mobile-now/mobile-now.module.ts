import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MobileNowRoutingModule } from './mobile-now-routing.module';
import { MobileNowComponent } from './mobile-now.component';
import { IntroComponent } from './intro/intro.component';
import { DevelopmenComponent } from './developmen/developmen.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { DevelopmenIosComponent } from './developmen-ios/developmen-ios.component';
import { Subjects2Component } from './subjects-2/subjects-2.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { Level1Component } from './level-1/level-1.component';
import { SharedModule } from '../shared/shared.module';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';



@NgModule({
  imports: [
    SharedModule,
    MobileNowRoutingModule,
    FlexLayoutModule,
    Ng2CarouselamosModule
  ],
  declarations: [MobileNowComponent, IntroComponent, DevelopmenComponent, SubjectsComponent, DevelopmenIosComponent, Subjects2Component, Level1Component]
})
export class MobileNowModule { }
