import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopmenIosComponent } from './developmen-ios.component';

describe('DevelopmenIosComponent', () => {
  let component: DevelopmenIosComponent;
  let fixture: ComponentFixture<DevelopmenIosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevelopmenIosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopmenIosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
