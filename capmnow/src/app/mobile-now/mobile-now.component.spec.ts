import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileNowComponent } from './mobile-now.component';

describe('MobileNowComponent', () => {
  let component: MobileNowComponent;
  let fixture: ComponentFixture<MobileNowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileNowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileNowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
