import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Subjects2Component } from './subjects-2.component';

describe('Subjects2Component', () => {
  let component: Subjects2Component;
  let fixture: ComponentFixture<Subjects2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Subjects2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Subjects2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
