import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatDefineUsComponent } from './what-define-us.component';

describe('WhatDefineUsComponent', () => {
  let component: WhatDefineUsComponent;
  let fixture: ComponentFixture<WhatDefineUsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatDefineUsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatDefineUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
