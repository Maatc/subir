import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-join-us',
  templateUrl: './join-us.component.html',
  styleUrls: ['./join-us.component.scss']
})
export class JoinUsComponent implements OnInit {

  teamMemberList = [
    {
      name: 'ILDA ATENCIA',
      position: 'FOUNDER',
      imageUrl: 'ilda-atencia.png'
    },
    {
      name: 'BRUNO RAMIREZ',
      position: 'CO-FOUNDER',
      imageUrl: 'bruno-ramirez.png'
    },
    {
      name: 'ENRIQUE ALVARADO',
      position: 'COORDINADOR DEL ÁREA DE CALIDAD',
      imageUrl: 'enrique.jpg'
    },/*
    {
      name: 'LUIS ANGEL RAMOS',
      position: 'COORDINADOR DEL ÁREA DE PRODUCTO',
      imageUrl: 'luis_angel.jpg'
    },
    {
      name: 'NOHOMI ALARCON',
      position: 'COORDINADORA DEL ÁREA COMERCIAL',
      imageUrl: 'nohomi.jpg'
    },
    {
      name: 'JULIA ESPINOZA',
      position: 'COORDINADORA DEL ÁREA DE SEGUIMIENTO EDUCATIVO',
      imageUrl: 'julia.jpg'
    }*/
  ]

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
  }

  getBackground(imageUrl: string){
    return this.sanitizer.bypassSecurityTrustStyle(`url(/assets/images/team-laos/${imageUrl})`);
  }

}
