import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsRoutingModule } from './us-routing.module';
import { UsComponent } from './us.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { IntroComponent } from './intro/intro.component';
import { WhatDefineUsComponent } from './what-define-us/what-define-us.component';
import { TeamComponent } from './team/team.component';
import { JoinUsComponent } from './join-us/join-us.component';
import { JoinUpComponent } from './join-up/join-up.component';
import { SharedModule } from '../shared/shared.module';
import { SwiperModule } from 'angular2-useful-swiper';
import { EquipoComponent } from './equipo/equipo.component';

@NgModule({
  imports: [
    CommonModule,
    UsRoutingModule,
    FlexLayoutModule,
    SharedModule,
    SwiperModule
  ],
  declarations: [UsComponent, IntroComponent, WhatDefineUsComponent, TeamComponent, JoinUsComponent, JoinUpComponent, EquipoComponent]
})
export class UsModule { }
