import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertificationNowRoutingModule } from './certification-now-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { IntroComponent } from './intro/intro.component';
import { CertificationNowComponent } from './certification-now.component';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';

@NgModule({
  imports: [
    SharedModule,
    CertificationNowRoutingModule,
    FlexLayoutModule,
    Ng2CarouselamosModule 
  ],
  declarations: [CertificationNowComponent, IntroComponent]
})
export class CertificationNowModule { }
