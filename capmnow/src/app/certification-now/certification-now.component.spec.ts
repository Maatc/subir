import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificationNowComponent } from './certification-now.component';

describe('CertificationNowComponent', () => {
  let component: CertificationNowComponent;
  let fixture: ComponentFixture<CertificationNowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificationNowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificationNowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
