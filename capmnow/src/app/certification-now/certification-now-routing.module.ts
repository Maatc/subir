import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CertificationNowComponent } from './certification-now.component';

const routes: Routes = [
  {
    path:'',
    component:CertificationNowComponent,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificationNowRoutingModule { }
