import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {

items:Array<any> =[];

constructor() { 
this.items =[
{ name:'/assets/images/productos/tentacionesnormal-01.jpg'},
{ name:'/assets/images/productos/tentacionesnormal-02.jpg'},
{ name:'/assets/images/productos/tentacionesnormal-03.jpg'},
{ name:'/assets/images/productos/tentacionesnormal-04.jpg'},
{ name:'/assets/images/productos/tentacionesnormal-05.jpg'},
{ name:'/assets/images/productos/tentacionesnormal-06.jpg'},
];

}

ngOnInit() {
}

}