import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PresentationRoutingModule } from './presentation-routing.module';
import { PresentationComponent } from './presentation.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    PresentationRoutingModule,
    FlexLayoutModule
  ],
  declarations: [PresentationComponent]
})
export class PresentationModule { }
