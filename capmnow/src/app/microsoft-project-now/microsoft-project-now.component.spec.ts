import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicrosoftProjectNowComponent } from './microsoft-project-now.component';

describe('MicrosoftProjectNowComponent', () => {
  let component: MicrosoftProjectNowComponent;
  let fixture: ComponentFixture<MicrosoftProjectNowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicrosoftProjectNowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicrosoftProjectNowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
