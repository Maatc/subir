import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MicrosoftProjectNowRoutingModule } from './microsoft-project-now-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { IntroComponent } from './intro/intro.component';
import { MicrosoftProjectNowComponent } from './microsoft-project-now.component';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';

@NgModule({
  imports: [
    SharedModule,
    MicrosoftProjectNowRoutingModule,
    FlexLayoutModule,
    Ng2CarouselamosModule
  ],
  declarations: [MicrosoftProjectNowComponent, IntroComponent]
})
export class MicrosoftProjectNowModule { }
