import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShellComponent } from './shell.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../shared/shared.module';
import { ComingSoonComponent } from './coming-soon/coming-soon.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    FlexLayoutModule,
    HttpClientModule
  ],
  declarations: [ShellComponent, HeaderComponent, FooterComponent, NotFoundComponent, ComingSoonComponent],
  exports: [
    ShellComponent,
    NotFoundComponent
  ]
})
export class ShellModule { }
