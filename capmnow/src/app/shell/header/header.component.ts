import { Component, OnInit, AfterViewInit } from '@angular/core';
declare var $;


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {

  initialized = false;

  constructor() { }

  ngOnInit() {
  }



  ngAfterViewInit(){
    $(document).on('scroll',()=>{
      let st = $(document).scrollTop();
      if(st >= 200){
        $('.header').addClass('header--fixed');
      }else{
        $('.header').removeClass('header--fixed');
      }
    })
    $('.header [scroll-to]').each(function(){
      let $el = $(this);
      $el.on('click',function(ev){
        ev.preventDefault();
        $(document).scrollTo($el.attr('scroll-to'),500,{easing: 'swing'});
      })
    });
    setTimeout(() => {
      this.initialized = true;
    }, 0);
  }


}
