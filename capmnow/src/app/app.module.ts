import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShellModule } from './shell/shell.module';
import { FlexLayoutModule, BreakPoint, BREAKPOINTS, 
  validateSuffixes, DEFAULT_BREAKPOINTS } from '@angular/flex-layout';

function updateBreakpoints(bp: BreakPoint) {
  switch(bp.alias) {
    case 'xs' : bp.mediaQuery =  '(max-width: 480px)';   break;
    case 'sm' : bp.mediaQuery =  '(min-width: 481px)'; break;
    case 'md' : bp.mediaQuery =  '(min-width: 768px)'; break;
    case 'lg' : bp.mediaQuery =  '(min-width: 992px)'; break;
    case 'sm' : bp.mediaQuery =  '(min-width: 1200px)'; break;
  }
  return bp;
}

@NgModule({
  declarations: [
    AppComponent
    ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ShellModule,
    Ng2CarouselamosModule
  ],
  providers: [
    {
      provide: BREAKPOINTS,
      useFactory: function customizeBreakPoints() {
        return validateSuffixes(DEFAULT_BREAKPOINTS.map(updateBreakpoints));
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
