import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './shell/not-found/not-found.component';
import { ComingSoonComponent } from './shell/coming-soon/coming-soon.component';
import { SubjectsComponent } from './subjects/subjects.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: 'app/presentation/presentation.module#PresentationModule'
  },
  {
    path: 'nosotros',
    loadChildren: 'app/us/us.module#UsModule'
  },
  {
    path: 'productos',
    loadChildren: 'app/methodology/methodology.module#MethodologyModule'
  },

  {
    path: 'servicios', 
    loadChildren: 'app/ninja-formation/ninja-formation.module#NinjaFormationModule'
  },
  {
    path: 'servicios/flotante', 
    redirectTo: '/servicios'
  },

  {
    path: 'servicios/flotante/tortaspedido',
    pathMatch: 'full',
    loadChildren: 'app/ninja-formation/ninja-formation.module#NinjaFormationModule'
  },
  {
    path: 'servicios/tortaspedido/flotante',
    redirectTo: '/servicios'
  },
  {
  path: 'servicios/diseñospersonalizados/flotante',
  redirectTo: '/servicios'
},
{
  path: 'servicios/fototortas/flotante',
  redirectTo: '/servicios'
},
{
  path: 'servicios/cupcake/flotante',
  redirectTo: '/servicios'
},
{
  path: 'servicios/dulceregalo/flotante',
  redirectTo: '/servicios'
},
{
  path: 'servicios/bocaditos/flotante',
  redirectTo: '/servicios'
},
  
  /*
  {
    path:'flotante',
    component:SubjectsComponent
     },
*/



  /*{
    path: 'servicios/flotante/tortaspedido',
    loadChildren: 'app/ninja-formation/ninja-formation.module#NinjaFormationModule'
  },*/

  


  {
    path: 'servicios/mobile-now/android',
    loadChildren: 'app/android-development/android-development.module#AndroidDevelopmentModule'
  },
 /* {
    path: 'cursos/mobile-now',
loadChildren: 'app/mobile-now/mobile-now.module#MobileNowModule'
  },*/
  {
    path: 'servicios/certification-now',
    loadChildren: 'app/certification-now/certification-now.module#CertificationNowModule'
  },
  {
    path: 'servicios/microsoft-project-now',
    loadChildren: 'app/microsoft-project-now/microsoft-project-now.module#MicrosoftProjectNowModule'
  },
  {
    path: 'formacion',
    loadChildren: 'app/ninja-formation/ninja-formation.module#NinjaFormationModule'
  },{
    path: 'testimonios',
    loadChildren: 'app/testimonials/testimonials.module#TestimonialsModule'
  },{
    path: 'contacto',
    loadChildren: 'app/join-us/join-us.module#JoinUsModule'
  },
  {
    path: 'proximamente',
    component: ComingSoonComponent
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/404'
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
