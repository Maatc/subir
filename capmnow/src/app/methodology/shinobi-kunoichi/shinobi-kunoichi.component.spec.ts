import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShinobiKunoichiComponent } from './shinobi-kunoichi.component';

describe('ShinobiKunoichiComponent', () => {
  let component: ShinobiKunoichiComponent;
  let fixture: ComponentFixture<ShinobiKunoichiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShinobiKunoichiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShinobiKunoichiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
