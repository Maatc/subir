import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MethodologyRoutingModule } from './methodology-routing.module';
import { MethodologyComponent } from './methodology.component';
import { IntroComponent } from './intro/intro.component';
import { DescriptionComponent } from './description/description.component';
import { FlowComponent } from './flow/flow.component';
import { BenefitsComponent } from './benefits/benefits.component';
import { ShinobiKunoichiComponent } from './shinobi-kunoichi/shinobi-kunoichi.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    MethodologyRoutingModule,
    FlexLayoutModule
  ],
  declarations: [MethodologyComponent, IntroComponent, DescriptionComponent, FlowComponent, BenefitsComponent, ShinobiKunoichiComponent]
})
export class MethodologyModule { }
