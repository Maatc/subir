import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkingTogetherComponent } from './working-together.component';

describe('WorkingTogetherComponent', () => {
  let component: WorkingTogetherComponent;
  let fixture: ComponentFixture<WorkingTogetherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkingTogetherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkingTogetherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
