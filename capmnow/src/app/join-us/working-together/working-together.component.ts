import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-working-together',
  templateUrl: './working-together.component.html',
  styleUrls: ['./working-together.component.scss']
})
export class WorkingTogetherComponent implements OnInit {

  contactFG: FormGroup;
  submitted: boolean = false;
  sending: boolean = false;
  error: boolean = false;
  success: boolean = false;
  fail: boolean = false;

  constructor(private fb: FormBuilder, private http: HttpClient) { 
    this.contactFG = this.fb.group({
      nombre: ['',[Validators.required]],
      email: ['',[Validators.email]],
      celular: ['',[]],
      comoNosConociste: ['',[]],
      queNecesitas: [null,[]],
      sobreTuProyecto: ['',[Validators.required]]
    });
  }

  ngOnInit() {
  }

  onSend(){
    if(this.contactFG.valid){
      this.error = false;
      this.http.post('http://138.197.122.110:75/capmnow-send-email', this.contactFG.value ).subscribe( success => {
        if(success){
          this.success = true;
          this.fail = false;
        }else{
          this.success = false;
          this.fail = true;
        }
      })
    }else{
      this.error = true;
    }
  }

}
