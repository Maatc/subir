import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
declare var $;

@Component({
  selector: 'app-join-us',
  templateUrl: './join-us.component.html',
  styleUrls: ['./join-us.component.scss']
})
export class JoinUsComponent implements OnInit, AfterViewInit {

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
  }

  ngAfterViewInit(){
    if(this.route.snapshot.queryParams['contact'] == '1'){
      $(document).scrollTo('.working-together',800,'easeInOutExpo');
    }
  }

}
