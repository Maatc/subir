import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinUsAsComponent } from './join-us-as.component';

describe('JoinUsAsComponent', () => {
  let component: JoinUsAsComponent;
  let fixture: ComponentFixture<JoinUsAsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinUsAsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinUsAsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
