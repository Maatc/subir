import { Component, OnInit } from '@angular/core';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  title: string = 'My first AGM project';
  lat: number = -12.0145978;
  lat2: number = -12.0149004;
  lat3: number = -12.014196;
  lat4: number = -12.0150694;
  lng: number = -77.101364;
  lng1: number = -77.101564;
  animation = null;

  constructor(private mapsAPILoader: MapsAPILoader) { 
    this.mapsAPILoader.load().then( () => {
      this.animation = 'BOUNCE';
    });
  }

  ngOnInit() {
  }

}
