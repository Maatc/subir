import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JoinUsRoutingModule } from './join-us-routing.module';
import { IntroComponent } from './intro/intro.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { MapComponent } from './map/map.component';
import { JoinUsAsComponent } from './join-us-as/join-us-as.component';
import { WorkingTogetherComponent } from './working-together/working-together.component';
import { JoinUsComponent } from './join-us.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AgmCoreModule } from '@agm/core';
import { environment } from '../../environments/environment.prod';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    JoinUsRoutingModule,
    FlexLayoutModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleMaps.apiKey
    })
  ],
  declarations: [IntroComponent, TestimonialsComponent, MapComponent, JoinUsAsComponent, WorkingTogetherComponent, JoinUsComponent]
})
export class JoinUsModule { }
