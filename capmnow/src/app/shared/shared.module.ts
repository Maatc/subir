import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollToDirective } from './directives/scroll-to.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ScrollToDirective
  ],
  exports: [
    CommonModule,
    ScrollToDirective,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
