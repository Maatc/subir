import { Directive, ElementRef, Input, OnInit } from '@angular/core';
declare var $;

@Directive({
  selector: '[scrollTo]'
})
export class ScrollToDirective implements OnInit {

  @Input('scrollTo') scrollToReference: number | string;

  constructor(private el: ElementRef) { 

  }

  ngOnInit(){
    let isNumber = false;
    try{
      parseInt(this.scrollToReference as string);
      isNumber = true;
    }catch(err){  }
    $(this.el.nativeElement).on('click',()=>{
      $(document).scrollTo(
        isNumber ? this.scrollToReference : $(this.scrollToReference)
        ,800,'easeInOutExpo');
    })
  }

}
